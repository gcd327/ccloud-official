import Main from './Main.vue'
import Home from './Home'
import Product from './Product'
import Price from './Price'
import Case from './Case'
import Docs from './Docs'
import Joint from './Joint'
import About from './About'
import Blog from './Blog'

export default {
  module: Main,
  children: [
    Home,
    Product,
    Price,
    Case,
    Docs,
    Joint,
    About,
    Blog
  ]
}
